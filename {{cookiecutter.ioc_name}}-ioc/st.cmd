# require statments should only specify the module name
# The version of the module shall be defined in the environment.yaml file
# Note that all modules shall be in lowercase
## Add extra modules here
{{ cookiecutter.require_mod }}

# Set EPICS environment variables
## Edit these as required for specific IOC requirements
## General IOC environment variables
epicsEnvSet("PREFIX", "{{ cookiecutter.prefix }}")
epicsEnvSet("DEVICE", "{{ cookiecutter.device }}")
epicsEnvSet("LOCATION", "{{ cookiecutter.location }}")
epicsEnvSet("ENGINEER", "{{ cookiecutter.full_name }} <{{ cookiecutter.email }}>")
## Add extra environment variables here

# Load standard module startup scripts
#iocshLoad("$(essioc_DIR)/essioc.iocsh")
iocshLoad("{{ cookiecutter.module_dir }}")

## Add extra startup scripts requirements here
# iocshLoad("$(module_DIR)/module.iocsh", "MACRO=MACRO_VALUE")

## Load custom databases
# cd $(E3_IOCSH_TOP)
# dbLoadRecords("db/custom_database1.db", "MACRO1=MACRO1_VALUE,...")
# dbLoadTemplate("db/custom_database2.substitutions", "MACRO1=MACRO1_VALUE,...")

# Call iocInit to start the IOC
iocInit()

## Add any post-iocInit statements here

